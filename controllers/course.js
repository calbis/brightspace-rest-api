const LMS = require("../services/lms");
const Axios = require("../helpers/axios").default();
const helper = require("../helpers/errorHandler");
const TEMPLATE_TYPE = process.env.LMS_TEMPLATE_TYPE_ID;
const COURSE_TYPE = process.env.LMS_COURSE_TYPE_ID;
const DateTime = require("luxon").DateTime;

exports.isCoursePresentByTemplate = function (parentLmsId, templateId) {
	return new Promise(function (resolve, reject) {
		isPresent(parentLmsId, templateId, TEMPLATE_TYPE)
			.then(function (templateLmsId) {
				if (templateLmsId !== null) {
					return exports.isCoursePresent(templateLmsId, templateId);
				} else {
					return null;
				}
			})
			.then(function (courseLmsId) {
				resolve(courseLmsId);
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

exports.isCoursePresent = function (templateId, courseId) {
	return isPresent(templateId, courseId);
};

function isPresent(templateId, courseId, orgType = COURSE_TYPE) {
	return new Promise(function (resolve, reject) {
		getCourseId(templateId, courseId, orgType)
			.then(function (id) {
				resolve(id);
			})
			.catch(function (err) {
				reject(err);
			});
	});
}

function getCourseId(templateId, courseId, orgType) {
	return new Promise(function (resolve, reject) {
		let url =
			"/d2l/api/lp/1.25/orgstructure/" + templateId + "/children/paged/";

		LMS.findFirstItemInPagedResult(url, "Code", courseId, "Identifier", orgType)
			.then(function (lmsCourseId) {
				resolve(lmsCourseId);
			})
			.catch(function (err) {
				reject(err);
			});
	});
}

exports.setCourseAsInactive = function (lmsCourseId) {
	return exports.updateCourse(lmsCourseId, null, null, false);
};

exports.addSandboxCourse = function (templateId, userId, courseId) {
	return new Promise(function (resolve, reject) {
		if (!userId) {
			reject({
				error: "User id not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl("POST", "/d2l/api/lp/1.25/courses/");
		let data = {
			Name: "Faculty Sandbox " + userId,
			Code: courseId,
			Path: "",
			CourseTemplateId: templateId,
			SemesterId: process.env.LMS_SANDBOX_SEMESTER_ID,
			StartDate: null,
			EndDate: null,
			LocaleId: null,
			ForceLocale: false,
			ShowAddressBook: true,
		};

		Axios.post(authedUrl, data)
			.then(function (res) {
				resolve({ id: res.data.Identifier, code: res.data.Code });
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.copyCourse = function (sourceLmsCourseId, destinationLmsCourseId) {
	return new Promise(function (resolve, reject) {
		if (!sourceLmsCourseId) {
			reject({
				error: "Source Course ID not provided",
			});
		}
		if (!destinationLmsCourseId) {
			reject({
				error: "Destination Course ID not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl(
			"POST",
			"/d2l/api/le/1.25/import/" + destinationLmsCourseId + "/copy/"
		);
		let data = {
			SourceOrgUnitId: sourceLmsCourseId,
		};

		Axios.post(authedUrl, data)
			.then(function (res) {
				resolve({ jobToken: res.data.JobToken });
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.addCourse = function (course) {
	return new Promise(function (resolve, reject) {
		if (!course) {
			reject({
				error: "Course not provided",
			});
		}

		let authedTemplateUrl = LMS.getFullApiUrl(
			"POST",
			"/d2l/api/lp/1.25/coursetemplates/"
		);

		let parents = [];
		course.parents.forEach(function (parent) {
			parents.push(Number(parent));
		});
		let templateData = {
			Name: course.name,
			Code: course.code,
			Path: "",
			ParentOrgUnitIds: parents,
		};
		let courseData = {
			Name: course.name,
			Code: course.code,
			Path: "",
			CourseTemplateId: null,
			SemesterId: course.semester,
			StartDate: course.startDate,
			EndDate: course.endDate,
			LocaleId: null,
			ForceLocale: false,
			ShowAddressBook: true,
		};

		Axios.post(authedTemplateUrl, templateData)
			.then(function (res) {
				courseData.CourseTemplateId = res.data.Identifier;
				let authedCourseUrl = LMS.getFullApiUrl(
					"POST",
					"/d2l/api/lp/1.25/courses/"
				);

				return Axios.post(authedCourseUrl, courseData);
			})
			.then(function (res) {
				resolve({ id: res.data.Identifier, code: res.data.Code });
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.addDevCourse = function (course, isMaster = false) {
	return new Promise(function (resolve, reject) {
		if (!course) {
			reject({
				error: "Course not provided",
			});
		}

		let errs = exports.getCourseErrors(false, course);
		if (errs.length > 0) {
			reject(errs);
		}

		let devCourseTemplateId = process.env.LMS_DEV_COURSE_TEMPLATE_ID;
		if (isMaster) {
			devCourseTemplateId = process.env.LMS_MASTER_COURSE_TEMPLATE_ID;
		}

		let courseData = {
			Name: course.name,
			Code: course.code,
			Path: "",
			CourseTemplateId: devCourseTemplateId,
			SemesterId: null,
			StartDate: null,
			EndDate: null,
			LocaleId: null,
			ForceLocale: false,
			ShowAddressBook: true,
		};

		let authedCourseUrl = LMS.getFullApiUrl(
			"POST",
			"/d2l/api/lp/1.25/courses/"
		);

		Axios.post(authedCourseUrl, courseData)
			.then(function (res) {
				resolve({ id: res.data.Identifier, code: res.data.Code });
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.isCoursePresentById = function (lmsId) {
	return new Promise(function (resolve) {
		getCourseById(lmsId)
			.then(function () {
				resolve(true);
			})
			.catch(function () {
				resolve(false);
			});
	});
};

function getCourseById(lmsId) {
	let authedGetUrl = LMS.getFullApiUrl(
		"GET",
		"/d2l/api/lp/1.25/courses/" + lmsId
	);

	return new Promise(function (resolve, reject) {
		Axios.get(authedGetUrl)
			.then(function (res) {
				resolve(res.data);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
}

exports.updateCourse = function (
	lmsId,
	courseCode = null,
	courseName = null,
	activeState = null,
	cascadeChanges = true,
	startDate = null,
	endDate = null
) {
	return new Promise(function (resolve, reject) {
		let authedPutUrl = LMS.getFullApiUrl(
			"PUT",
			"/d2l/api/lp/1.25/courses/" + lmsId
		);
		let courseCodeFromLMS = null;
		let courseTemplateLmsId = null;

		getCourseById(lmsId)
			.then(function (courseInfo) {
				courseCodeFromLMS = courseInfo.Code;
				courseTemplateLmsId = courseInfo.CourseTemplate.Identifier;

				let data = {
					Name: courseInfo.Name,
					Code: courseInfo.Code,
					StartDate: courseInfo.StartDate,
					EndDate: courseInfo.EndDate,
					IsActive: courseInfo.IsActive,
					Description: courseInfo.Description,
				};

				if (courseCode !== null) {
					data.Code = courseCode;
				}
				if (courseName !== null) {
					data.Name = courseName;
				}
				if (activeState !== null) {
					data.IsActive = activeState;
				}
				if (startDate !== null) {
					data.StartDate = startDate;
				}
				if (endDate !== null) {
					data.EndDate = endDate;
				}

				return Axios.put(authedPutUrl, data);
			})
			.then(function () {
				if (courseCode === null && courseName === null) {
					return resolve();
				}

				if (cascadeChanges === true) {
					let authedPutTemplateUrl = LMS.getFullApiUrl(
						"PUT",
						"/d2l/api/lp/1.25/coursetemplates/" + courseTemplateLmsId
					);
					let data = {};
					if (courseCode !== null) {
						data.Code = courseCode;
					}
					if (courseName !== null) {
						data.Name = courseName;
					}

					return Axios.put(authedPutTemplateUrl, data);
				}
			})
			.then(function () {
				resolve({ id: lmsId, code: courseCodeFromLMS });
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.getCoursesByTemplate = function (templateId) {
	return new Promise(async function (resolve, reject) {
		if (!templateId) {
			return reject({
				error: "Template id not provided",
			});
		}

		let url =
			"/d2l/api/lp/1.25/orgstructure/" + templateId + "/children/paged/";
		let bookmark = null;
		let hasMore = true;
		let courses = [];

		while (hasMore) {
			try {
				let data = await LMS.getPagedResult(url, bookmark, COURSE_TYPE);

				bookmark = data.PagingInfo.Bookmark;
				hasMore = data.PagingInfo.HasMoreItems;

				data.Items.forEach(function (item) {
					courses.push({
						id: item.Identifier,
						code: item.Code,
					});
				});

				if (!hasMore) {
					return resolve(courses);
				}
			} catch (e) {
				return reject(e);
			}
		}
	});
};

exports.getCourseData = function (courseLmsId) {
	return new Promise(function (resolve, reject) {
		if (!courseLmsId) {
			reject({
				error: "Course LMS id not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl(
			"GET",
			"/d2l/api/lp/1.25/courses/" + courseLmsId
		);
		Axios.get(authedUrl)
			.then(function (res) {
				resolve(res.data);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.getCourseFinalGrades = function (courseLmsId) {
	return new Promise(function (resolve, reject) {
		if (!courseLmsId) {
			reject({
				error: "Course LMS id not provided",
			});
		}

		let url = "/d2l/api/le/1.41/" + courseLmsId + "/grades/final/values/";
		LMS.getAllItemsFromPagedObjectList(url, 1000)
			.then(function (data) {
				let grades = simplifyGrades(data);

				resolve(grades);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

function simplifyGrades(lmsGrades) {
	let simplifiedGrades = [];
	for (let i = 0; i < lmsGrades.length; i++) {
		let grade = {
			id: lmsGrades[i].User.Identifier,
			userId: lmsGrades[i].User.OrgDefinedId,
			displayedGrade: null,
			pointsNumerator: null,
			pointsDenominator: null,
			lastModified: null,
		};

		if (lmsGrades[i].GradeValue !== null) {
			grade.displayedGrade = lmsGrades[i].GradeValue.DisplayedGrade;
			grade.pointsNumerator = lmsGrades[i].GradeValue.PointsNumerator;
			grade.pointsDenominator = lmsGrades[i].GradeValue.PointsDenominator;
			grade.lastModified = lmsGrades[i].GradeValue.LastModified;
		}

		simplifiedGrades.push(grade);
	}

	return simplifiedGrades;
}

exports.getCourseErrors = function (isAdd, course) {
	let errs = [];
	if (isAdd) {
		if (!course.hasOwnProperty("semester")) {
			errs.push({
				error: "Course semester missing",
			});
		}
		if (!course.hasOwnProperty("master")) {
			errs.push({
				error: "Course master course missing",
			});
		}
		if (!course.hasOwnProperty("parents")) {
			errs.push({
				error: "Course parents missing",
			});
		} else {
			if (!Array.isArray(course.parents)) {
				errs.push({
					error: "Course Parents is not an array",
				});
			}
		}
		if (!course.hasOwnProperty("name")) {
			errs.push({
				error: "Course name missing",
			});
		}
		if (!course.hasOwnProperty("code")) {
			errs.push({
				error: "Course code missing",
			});
		}
	}

	if (course.hasOwnProperty("startDate") && !validDate(course.startDate)) {
		errs.push({
			error: "Course startDate is in the wrong format",
		});
	}
	if (course.hasOwnProperty("endDate") && !validDate(course.endDate)) {
		errs.push({
			error: "Course endDate is in the wrong format",
		});
	}

	return errs;
};

function validDate(date) {
	let re = /^\d{4}-\d{2}-\d{2}$/;

	return re.test(date);
}

exports.getCourseEnrollments = function (courseLmsId) {
	if (!courseLmsId) {
		Promise.reject({
			error: "Course LMS id not provided",
		});
	}

	let url = "/d2l/api/le/1.34/" + courseLmsId + "/classlist/paged/";
	return LMS.getAllItemsFromPagedObjectList(url, 500);
};

exports.getCourseEnrollmentsWithRoleName = function (courseLmsId) {
	return new Promise(function (resolve, reject) {
		if (!courseLmsId) {
			return reject({
				error: "Course LMS id not provided",
			});
		}

		let instructorRoleId = process.env.LMS_INSTRUCTOR_ROLE_ID;
		let newHireInstructorRoleId = process.env.LMS_NEW_HIRE_INSTRUCTOR_ROLE_ID;
		let learnerRoleId = process.env.LMS_STUDENT_ROLE_ID;
		let incompleteStudentRoleId = process.env.LMS_INCOMPLETE_STUDENT_ROLE_ID;
		let teachingAssistantRoleId = process.env.LMS_TEACHING_ASSISTANT_ROLE_ID;
		exports
			.getCourseEnrollments(courseLmsId)
			.then(function (enrollments) {
				enrollments.map(function (e) {
					switch (e.RoleId) {
						case parseInt(instructorRoleId):
							e.RoleName = "Instructor";
							break;
						case parseInt(newHireInstructorRoleId):
							e.RoleName = "Instructor - New Hire";
							break;
						case parseInt(learnerRoleId):
							e.RoleName = "Learner";
							break;
						case parseInt(incompleteStudentRoleId):
							e.RoleName = "Incomplete Student";
							break;
						case parseInt(teachingAssistantRoleId):
							e.RoleName = "Teaching Assistant";
							break;
						default:
							e.RoleName = "Other";
					}
				});

				resolve(enrollments);
			})
			.catch(function (error) {
				reject(error);
			});
	});
};

exports.getCourseGradesByColumn = function (courseLmsId, gradeBookColumnLmsId) {
	return new Promise(function (resolve, reject) {
		let url =
			"/d2l/api/le/1.34/" +
			courseLmsId +
			"/grades/" +
			gradeBookColumnLmsId +
			"/values/";

		LMS.getAllItemsFromPagedObjectList(url)
			.then(function (data) {
				let grades = simplifyGrades(data);

				resolve(grades);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.deleteCourseTemplate = function (templateId) {
	return new Promise(function (resolve, reject) {
		if (!templateId) {
			reject({
				error: "Course template id not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl(
			"DELETE",
			"/d2l/api/lp/1.25/coursetemplates/" + templateId
		);
		Axios.delete(authedUrl)
			.then(function (res) {
				resolve();
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.deleteCourse = function (courseId) {
	return new Promise(function (resolve, reject) {
		if (!courseId) {
			reject({
				error: "Course id not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl(
			"DELETE",
			"/d2l/api/lp/1.25/courses/" + courseId
		);
		Axios.delete(authedUrl)
			.then(function (res) {
				resolve();
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};
exports.delete = function (courseId) {
	//todo: logging
	return new Promise(function (resolve, reject) {
		exports
			.getCourseData(courseId)
			.then(function (course) {
				let courseTemplate = course["CourseTemplate"];
				if (courseTemplate == null) {
					// no course template exist, delete course directly
					exports
						.deleteCourse(courseId)
						.then(function () {
							resolve({
								Identifier: course.Identifier,
								Code: course.Code,
								Name: course.Name,
							});
						})
						.catch(function (err) {
							reject(err);
						});
				} else {
					let templateId = course["CourseTemplate"]["Identifier"];
					exports
						.deleteCourseTemplate(templateId)
						.then(function (deletedCourseTemplate) {
							exports
								.deleteCourse(courseId)
								.then(function () {
									resolve({
										Identifier: course.Identifier,
										Code: course.Code,
										Name: course.Name,
									});
								})
								.catch(function (err) {
									// todo: err type
									reject(helper.handleError(err));
								});
						})
						.catch(function (err) {
							//course template already been deleted, delete course itself directly
							if (err.status === 404 || err.status === 403) {
								exports
									.deleteCourse(courseId)
									.then(function () {
										resolve({
											Identifier: course.Identifier,
											Code: course.Code,
											Name: course.Name,
										});
									})
									.catch(function (err) {
										// todo: err type
										reject(helper.handleError(err));
									});
							}
							reject(helper.handleError(err));
						});
				}
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.setDefaultCourseProperties = function (courseData) {
	let activeState = null;
	if (
		courseData.hasOwnProperty("isActive") &&
		String(courseData.isActive) === "false"
	) {
		activeState = false;
	} else if (
		courseData.hasOwnProperty("isActive") &&
		String(courseData.isActive) === "true"
	) {
		activeState = true;
	}
	courseData.isActive = activeState;

	if (!courseData.hasOwnProperty("startDate")) {
		courseData.startDate = null;
	} else {
		courseData.startDate = formatDateForLMS(courseData.startDate);
	}

	if (!courseData.hasOwnProperty("endDate")) {
		courseData.endDate = null;
	} else {
		courseData.endDate = formatDateForLMS(courseData.endDate);
	}

	if (!courseData.hasOwnProperty("name")) {
		courseData.name = null;
	}

	if (!courseData.hasOwnProperty("code")) {
		courseData.code = null;
	}
};

function formatDateForLMS(date) {
	let lDate = DateTime.fromISO(date, { zone: "America/New_York" });

	return lDate.toUTC().toISO();
}
