const Axios = require("../helpers/axios").default();
const LMS = require("../services/lms");
const helper = require("../helpers/errorHandler");

exports.getUserId = function (userId) {
	return new Promise(function (resolve, reject) {
		getUserId(userId)
			.then(function ReturnUserId(userId) {
				resolve(userId);
			})
			.catch(function ReportAnyErrors(errs) {
				reject(errs);
			});
	});
};

exports.isUserPresent = function (userId) {
	return new Promise(function (resolve, reject) {
		getUserId(userId)
			.then(function (id) {
				resolve(id);
			})
			.catch(function () {
				resolve(null);
			});
	});
};

function getUserId(userId) {
	return new Promise(function (resolve, reject) {
		let authedUrl = LMS.getFullApiUrl(
			"GET",
			"/d2l/api/lp/1.25/users/?orgDefinedId=" + userId
		);

		Axios.get(authedUrl)
			.then(function (res) {
				resolve(res.data[0].UserId);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
}

exports.add = function (user) {
	return new Promise(function (resolve, reject) {
		if (!user) {
			return reject({
				error: "User not provided",
			});
		}

		getUserErrors(user)
			.then(function AddUserToLMS(user) {
				return addUserToLMS(user);
			})
			.then(function ReturnUserId(userId) {
				resolve(userId);
			})
			.catch(function ReportAnyErrors(errs) {
				reject(errs);
			});
	});
};

exports.update = function (lmsId, user) {
	return new Promise(function (resolve, reject) {
		if (!user) {
			return reject({
				error: "User not provided",
			});
		}

		getUserErrors(user)
			.then(function UpdateUserInLMS() {
				return updateUserInLMS(lmsId, user);
			})
			.then(function ReturnUserId(userId) {
				resolve(userId);
			})
			.catch(function ReportAnyErrors(errs) {
				reject(errs);
			});
	});
};

function updateUserInLMS(lmsId, user) {
	return new Promise(function (resolve, reject) {
		let authedUrl = LMS.getFullApiUrl("PUT", "/d2l/api/lp/1.25/users/" + lmsId);
		let data = {
			OrgDefinedId: user.userId,
			FirstName: user.firstName,
			MiddleName: "",
			LastName: user.lastName,
			ExternalEmail: user.email,
			UserName: user.username,
			Activation: {
				IsActive: true,
			},
		};
		if (user.middleName) {
			data.MiddleName = user.middleName;
		}

		Axios.put(authedUrl, data)
			.then(function (res) {
				resolve(res.data.UserId);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
}

function addUserToLMS(user) {
	return new Promise(function (resolve, reject) {
		let authedUrl = LMS.getFullApiUrl("POST", "/d2l/api/lp/1.25/users/");
		let data = {
			OrgDefinedId: user.userId,
			FirstName: user.firstName,
			MiddleName: "",
			LastName: user.lastName,
			ExternalEmail: user.email,
			UserName: user.username,
			RoleId: process.env.LMS_USER_ROLE_ID,
			IsActive: true,
			SendCreationEmail: false,
		};
		if (user.middleName) {
			data.MiddleName = user.middleName;
		}

		Axios.post(authedUrl, data)
			.then(function (res) {
				resolve(res.data.UserId);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
}

function getUserErrors(user) {
	let errs = [];
	if (!user.hasOwnProperty("firstName")) {
		errs.push({
			error: "first name missing",
		});
	}
	if (!user.hasOwnProperty("lastName")) {
		errs.push({
			error: "last name missing",
		});
	}
	if (!user.hasOwnProperty("username")) {
		errs.push({
			error: "username missing",
		});
	}
	if (!user.hasOwnProperty("email")) {
		errs.push({
			error: "email missing",
		});
	}
	if (!user.hasOwnProperty("userId")) {
		errs.push({
			error: "userId missing",
		});
	}
	if (!user.hasOwnProperty("primaryCampus")) {
		errs.push({
			error: "primary campus missing",
		});
	}
	if (!user.hasOwnProperty("campuses")) {
		errs.push({
			error: "campuses missing",
		});
	} else {
		if (!Array.isArray(user.campuses)) {
			errs.push({
				error: "campuses is not an array",
			});
		} else {
		}
	}

	return new Promise(function (resolve, reject) {
		if (errs.length > 0) {
			reject(errs);
		} else {
			resolve(user);
		}
	});
}
exports.getUser = function (userLmsId) {
	return new Promise(function (resolve, reject) {
		if (!userLmsId) {
			reject({
				error: "User LMS id not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl(
			"GET",
			"/d2l/api/lp/1.25/users/" + userLmsId
		);
		Axios.get(authedUrl)
			.then(function (res) {
				resolve(res.data);
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.getPreviewUserId = function (userLmsId) {
	return new Promise(function (resolve, reject) {
		exports
			.getUser(userLmsId)
			.then(function (userData) {
				return exports.getUserId(
					userData.OrgDefinedId + process.env.PREVIEW_USER_POSTFIX
				);
			})
			.then(function (previewUserLmsId) {
				resolve(previewUserLmsId);
			})
			.catch(function (err) {
				reject(err);
			});
	});
};
