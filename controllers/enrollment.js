const Axios = require("../helpers/axios").default();
const LMS = require("../services/lms");
const helper = require("../helpers/errorHandler");
const bb = require("bluebird");
const userController = require("../controllers/user");
const logger = require("../services/logger");
const asyncPool = require("tiny-async-pool");

exports.add = function (enrollment) {
	return new Promise(function (resolve, reject) {
		if (!enrollment) {
			reject({
				error: "Enrollment not provided",
			});
		}

		let errs = getEnrollmentErrors(enrollment);
		if (errs.length > 0) {
			reject(errs);
		}

		const roleId = exports.getRoleIdByCode(enrollment.type);

		let enrollData = null;

		exports
			.addSystemEnrollmentWithoutClobber(
				enrollment.courseLmsId,
				enrollment.userLmsId,
				roleId
			)
			.then(function (enrollDataIds) {
				enrollData = enrollDataIds;
			})
			.then(function enrollPreviewUserIfNeeded() {
				if (enrollment.type === "T" || enrollment.type === "I") {
					return userController
						.getPreviewUserId(enrollment.userLmsId)
						.then(function (previewUserLmsId) {
							return exports.addSystemEnrollment(
								enrollment.courseLmsId,
								previewUserLmsId,
								process.env.LMS_PREVIEW_USER_ROLE_ID
							);
						});
				}
			})
			.then(function () {
				resolve({
					courseId: enrollData.orgId,
					userId: enrollData.userId,
					roleId: enrollData.roleId,
				});
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

exports.getRoleIdByCode = function (roleCode) {
	if (roleCode === "S") {
		return process.env.LMS_STUDENT_ROLE_ID;
	} else if (roleCode === "X") {
		return process.env.LMS_INCOMPLETE_STUDENT_ROLE_ID;
	} else if (roleCode === "T") {
		return process.env.LMS_TEACHING_ASSISTANT_ROLE_ID;
	} else if (roleCode === "N") {
		return process.env.LMS_NEW_HIRE_INSTRUCTOR_ROLE_ID;
	} else if (roleCode === "P") {
		return process.env.LMS_INCARCERATED_STUDENT_ROLE_ID;
	} else if (roleCode === "Q") {
		return process.env.LMS_INCARCERATED_INCOMPLETE_STUDENT_ROLE_ID;
	}

	return process.env.LMS_INSTRUCTOR_ROLE_ID;
};

exports.addSystemEnrollmentWithoutClobber = function (
	orgUnitId,
	userId,
	roleId
) {
	return new Promise(function (resolve, reject) {
		if (roleId === process.env.LMS_TEACHING_ASSISTANT_ROLE_ID) {
			exports
				.getUserRoleId(orgUnitId, userId)
				.then(function (currentRoleId) {
					if (
						currentRoleId === null ||
						currentRoleId === parseInt(process.env.LMS_STUDENT_ROLE_ID) ||
						currentRoleId ===
							parseInt(process.env.LMS_INCOMPLETE_STUDENT_ROLE_ID) ||
						currentRoleId ===
							parseInt(process.env.LMS_NEW_HIRE_INSTRUCTOR_ROLE_ID)
					) {
						return resolve(
							exports.addSystemEnrollment(orgUnitId, userId, roleId)
						);
					} else {
						return resolve({
							orgId: orgUnitId,
							userId: userId,
							roleId: currentRoleId,
						});
					}
				})
				.catch(function (err) {
					return reject(err);
				});
		} else {
			return resolve(exports.addSystemEnrollment(orgUnitId, userId, roleId));
		}
	});
};

exports.enrollUserInCampusOrgs = function (userLmsId) {
	return new Promise(function (resolve, reject) {
		let umsId = process.env.LMS_UMS_ORG_ID;
		let orgIdsEnv = process.env.LMS_ENROLL_USER_ORGS;
		let orgIds = orgIdsEnv.split(",");

		exports
			.getUserRoleId(umsId, userLmsId)
			.then(function (userRoleId) {
				if (userRoleId !== null) {
					return bb.map(
						orgIds,
						function (orgId) {
							return exports.addSystemEnrollment(orgId, userLmsId, userRoleId);
						},
						{
							concurrency: 3,
						}
					);
				}
			})
			.then(function () {
				resolve(userLmsId);
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

exports.addSystemEnrollment = function (orgUnitId, userId, roleId) {
	return new Promise(function (resolve, reject) {
		let data = {
			OrgUnitId: orgUnitId,
			UserId: userId,
			RoleId: roleId,
		};

		let authedUrl = LMS.getFullApiUrl("POST", "/d2l/api/lp/1.25/enrollments/");
		Axios.post(authedUrl, data)
			.then(function (res) {
				resolve({
					orgId: res.data.OrgUnitId,
					userId: res.data.UserId,
					roleId: res.data.RoleId,
					isCascading: res.data.IsCascading,
				});
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

exports.delete = function (enrollment) {
	return new Promise(function (resolve, reject) {
		if (!enrollment) {
			reject({
				error: "Un-enrollment not provided",
			});
		}

		let errs = getEnrollmentErrorsDelete(enrollment);
		if (errs.length > 0) {
			reject(errs);
		}

		let resEnrollData = null;
		exports
			.deleteSystemEnrollment(enrollment)
			.then(function (enrollmentData) {
				resEnrollData = enrollmentData;
			})
			.then(function deletePreviewUserIfNeeded() {
				if (
					resEnrollData.roleId.toString() ===
						process.env.LMS_INSTRUCTOR_ROLE_ID ||
					resEnrollData.roleId.toString() ===
						process.env.LMS_NEW_HIRE_INSTRUCTOR_ROLE_ID ||
					resEnrollData.roleId.toString() ===
						process.env.LMS_TEACHING_ASSISTANT_ROLE_ID
				) {
					return userController
						.getPreviewUserId(enrollment.userLmsId)
						.then(function (previewUserLmsId) {
							return exports.deleteSystemEnrollment({
								courseLmsId: resEnrollData.courseId,
								userLmsId: previewUserLmsId,
							});
						})
						.catch(function (err) {
							logger.warn(
								"Swallowing catch error on deleting a preview user enrollment",
								{ resEnrollData: resEnrollData, err: err }
							);
						});
				}
			})
			.then(function () {
				resolve(resEnrollData);
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

exports.deleteSystemEnrollment = function (enrollment) {
	return new Promise(function (resolve, reject) {
		let authedUrl = LMS.getFullApiUrl(
			"DELETE",
			"/d2l/api/lp/1.25/enrollments/orgUnits/" +
				enrollment.courseLmsId +
				"/users/" +
				enrollment.userLmsId
		);
		Axios.delete(authedUrl)
			.then(function (res) {
				resolve({
					courseId: res.data.OrgUnitId,
					userId: res.data.UserId,
					roleId: res.data.RoleId,
				});
			})
			.catch(function (err) {
				reject(helper.handleError(err));
			});
	});
};

function getEnrollmentErrors(enrollment) {
	let errs = getEnrollmentErrorsDelete(enrollment);

	if (!enrollment.hasOwnProperty("type")) {
		errs.push({
			error: "type missing",
		});
	} else {
		if (
			!(
				enrollment.type === "S" ||
				enrollment.type === "I" ||
				enrollment.type === "N" ||
				enrollment.type === "X" ||
				enrollment.type === "T" ||
				enrollment.type === "P" ||
				enrollment.type === "Q"
			)
		) {
			errs.push({
				error:
					"type can only be S (Student) or I (Instructor) or N(Instructor - New Hire) or X (Incomplete Student) or T (Teaching Assistant)",
			});
		}
	}

	return errs;
}

function getEnrollmentErrorsDelete(enrollment) {
	let errs = [];
	if (!enrollment.hasOwnProperty("userLmsId")) {
		errs.push({
			error: "user lms id missing",
		});
	}
	if (!enrollment.hasOwnProperty("courseLmsId")) {
		errs.push({
			error: "course lms id missing",
		});
	}

	return errs;
}

exports.getUserRoleId = function (orgLmsId, userLmsId) {
	return new Promise(function (resolve, reject) {
		exports
			.getUserRole(orgLmsId, userLmsId)
			.then(function (role) {
				if (role === null) {
					return resolve(null);
				}

				return resolve(role.RoleId);
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

exports.getUserRole = function (orgLmsId, userLmsId) {
	return new Promise(function (resolve, reject) {
		let authedUrl = LMS.getFullApiUrl(
			"GET",
			"/d2l/api/lp/1.25/enrollments/orgUnits/" +
				orgLmsId +
				"/users/" +
				userLmsId
		);
		Axios.get(authedUrl)
			.then(function (res) {
				resolve(res.data);
			})
			.catch(function (err) {
				if (err && err.response && err.response.status === 404) {
					resolve(null);
				} else {
					reject(helper.handleError(err));
				}
			});
	});
};

exports.getEnrollmentsByOrganizationType = function (orgTypeId, userLmsId) {
	let url = "/d2l/api/lp/1.25/enrollments/users/" + userLmsId + "/orgUnits/";
	let queryParams = {};
	queryParams.orgUnitTypeId = orgTypeId;

	return LMS.getAllItemsFromPagedResultSet(url, queryParams);
};

exports.getAllUserEnrollments = function (userLmsId) {
	let url = "/d2l/api/lp/1.25/enrollments/users/" + userLmsId + "/orgUnits/";

	return LMS.getAllItemsFromPagedResultSet(url);
};

exports.cascadeDeleteEnrollments = function (userLmsId, orgId) {
	return new Promise(function (resolve) {
		exports
			.deleteSystemEnrollmentWithoutError({
				courseLmsId: orgId,
				userLmsId: userLmsId,
			})
			.then(function () {
				// We need to wait for the cascading role to remove the enrollment record from all the orgs in the system.
				// Otherwise, we'll just be trying to delete non-existent enrollment records
				return sleep(Number(process.env.CASCADING_DELETE_SLEEP_TIME));
			})
			.then(function () {
				return exports.getAllUserEnrollments(userLmsId);
			})
			.then(function (enrollments) {
				return asyncPool(1, enrollments, function (e) {
					return exports.deleteSystemEnrollmentWithoutError({
						courseLmsId: e.OrgUnit.Id,
						userLmsId: userLmsId,
					});
				});
			})
			.catch(function () {
				return resolve();
			})
			.finally(function () {
				return resolve();
			});
	});
};

exports.deleteSystemEnrollmentWithoutError = function (enrollment) {
	return new Promise(function (resolve) {
		exports
			.deleteSystemEnrollment(enrollment)
			.then(function () {
				return resolve();
			})
			.catch(function () {
				return resolve();
			});
	});
};

const sleep = (ms) => new Promise((res) => setTimeout(res, ms));
