const Axios = require("../helpers/axios").default();
const httpAdapter = require("axios/lib/adapters/http");
const urlHelper = require("url");
const logger = require("../services/logger");

const LMS = require("../services/lms");
const helper = require("../helpers/errorHandler");

function getDownloadUrl(plugin, key) {
	logger.info("Building url for plugin " + plugin + " and key " + key);
	return new Promise(function (resolve, reject) {
		exports
			.getPluginDetails(plugin)
			.then(function (response) {
				let returnUrl =
					"/d2l/api/lp/1.25/dataexport/bds/" + response["PluginId"] + "/" + key;
				console.log(returnUrl);
				resolve(returnUrl);
			})
			.catch(function (err) {
				reject(err);
			});
	});
}

exports.getPluginFile = function (plugin, key) {
	return new Promise(async function (resolve, reject) {
		getDownloadUrl(plugin, key)
			.then(function (url) {
				logger.info("URL: " + url);

				let authedUrl = LMS.getFullApiUrl("GET", url);
				Axios.get(authedUrl, {
					responseType: "stream",
					adapter: httpAdapter,
				})
					.then(function (response) {
						return resolve(response);
					})
					.catch(function (err) {
						logger.error(err);
						reject(helper.handleError(err));
					});
			})

			.catch(function (err) {
				reject(err);
			});
	});
};

exports.getDownloadsAvailableByPlugin = function (plugin) {
	let url = "/d2l/api/lp/1.25/dataexport/bds?pageSize=500";

	return new Promise(function (resolve, reject) {
		if (!plugin) {
			reject({
				error: "Data Hub plugin not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl("GET", url);
		Axios.get(authedUrl)
			.then(function (res) {
				const filtered = res.data["BrightspaceDataSets"].filter(
					(item) => item["Name"].toLowerCase() === normalizePlugin(plugin)
				);
				if (filtered.length > 0) {
					return resolve(filtered[0]);
				}
				return resolve(null);
			})
			.catch(function (err) {
				reject("There was an error: " + err);
			});
	});
};

exports.getPluginDetails = function (plugin) {
	let url = "/d2l/api/lp/1.25/dataexport/bds/list";

	return new Promise(function (resolve, reject) {
		if (!plugin) {
			reject({
				error: "Data Hub plugin not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl("GET", url);
		Axios.get(authedUrl)
			.then(function (res) {
				const filtered = res.data.filter(
					(item) => item["Name"].toLowerCase() === normalizePlugin(plugin)
				);
				if (filtered.length > 0) {
					return resolve(filtered[0]);
				}
				return resolve(null);
			})
			.catch(function (err) {
				reject("There was an error: " + err);
			});
	});
};

exports.getPluginDataAsOf = function (plugin, ts) {
	return new Promise(function (resolve, reject) {
		if (!Date.parse(ts)) {
			reject({ error: "Invalid timestamp" });
		}
	});

	return new Promise(function (resolve, reject) {
		if (!plugin) {
			reject({
				error: "Data Hub plugin not provided",
			});
		}

		let authedUrl = LMS.getFullApiUrl("GET", url);
		Axios.get(authedUrl)
			.then(function (res) {
				const filtered = res.data.filter(
					(item) => item["Name"].toLowerCase() === normalizePlugin(plugin)
				);
				if (filtered.length > 0) {
					return resolve(filtered[0]);
				}
				return resolve(null);
			})
			.catch(function (err) {
				reject("There was an error: " + err);
			});
	});
};

function normalizePlugin(plugin) {
	if (typeof plugin !== "string") {
		return "";
	}
	return plugin.split("_").join(" ").toLowerCase();
}
