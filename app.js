const express = require("express");
const cookieParser = require("cookie-parser");
const expressLogger = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const jwt = require("express-jwt");
const rlValues = require("./helpers/axios").rateLimitValues;

const indexRouter = require("./routes/index");
const courseRouter = require("./routes/course");
const userRouter = require("./routes/user");
const enrollmentRouter = require("./routes/enrollment");
const semesterRouter = require("./routes/semester");
const sandboxRouter = require("./routes/sandbox");
const organizationRouter = require("./routes/organization");
const datahubRouter = require("./routes/datahub");
const roleRouter = require("./routes/role");

const app = express();

app.use(helmet());
app.use(expressLogger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(
	jwt({
		secret: process.env.JWT_SECRET,
		audience: process.env.APP_HOST,
		algorithms: ["HS256"],
	}).unless({
		custom: function (req) {
			return req.url === "/" && req.originalUrl === "/" && req.method === "GET";
		},
	})
);

app.use(function (req, res, next) {
	let user = "none";
	if (req.user) {
		user = req.user.sub;
	}
	next();
});

app.use(function (req, res, next) {
	res.append("X-Rate-Limit-Remaining", rlValues.rlRemain());
	res.append("X-Rate-Limit-Reset", rlValues.rlReset());
	res.append("X-Rate-Limit-State", rlValues.appState());

	next();
});

app.use("/", indexRouter);
app.use("/course", courseRouter);
app.use("/user", userRouter);
app.use("/enrollment", enrollmentRouter);
app.use("/semester", semesterRouter);
app.use("/sandbox", sandboxRouter);
app.use("/organization", organizationRouter);
app.use("/datahub", datahubRouter);
app.use("/role", roleRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	res.sendStatus(404);
});

// error handler
app.use(function (err, req, res) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("NODE_ENV") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.send(res.locals);
});

module.exports = app;
