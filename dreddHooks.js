const hooks = require("hooks");

hooks.beforeAll(function (transactions) {
	transactions.forEach(function (t) {
		t.request.headers["Authorization"] =
			"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb2NhbGhvc3QiLCJzdWIiOiJkcmVkZCIsImlzcyI6InNyLWExNWJhMjgyLmxhLnVzbS5tYWluZS5lZHUiLCJpYXQiOjE1NzYwMTIyMjZ9.iHC8UgbPDj6eY3ygOAr0n6mMWvHbEN8p661q18D2BbQ";
	});
});
