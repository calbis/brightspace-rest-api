#!/bin/sh

tail -F /home/node/app/logs/log.log | while read line; do
    url="https://${SPLUNK_HOST}:8088/services/collector/event"
    auth="Authorization: Splunk ${SPLUNK_TOKEN}"
    event='{"host": "'$HOSTNAME'","sourcetype": "_json","index": "'${SPLUNK_INDEX}'", "source": "rest_api","event":'
    event=$event$line
    event=$event"}"
    (curl -sg -k "$url" -H "$auth" -d "$event")
done

echo "Splunk logging tail finished"
echo "Splunk logging tail finished" >> /home/node/app/logs/errorLog.log
