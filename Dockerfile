FROM node:14-alpine

RUN apk add --no-cache curl

WORKDIR /home/node/bin
COPY splunk.sh ./
COPY docker/docker-entrypoint.sh ./
RUN chmod +x ./*.sh

WORKDIR /home/node/app
COPY bin ./bin/
COPY controllers ./controllers/
COPY helpers ./helpers/
RUN mkdir logs \
    && echo '{"server": "init"}' > logs/log.log
COPY routes ./routes/
COPY services ./services/
COPY app.js ./
COPY .env ./
COPY package.json ./
COPY yarn.lock ./

RUN yarn --non-interactive --no-progress --prod --silent

RUN chown node:node -R /home/node

USER node

ENTRYPOINT ["/home/node/bin/docker-entrypoint.sh"]
