const jwt = require("jsonwebtoken");
const os = require("os");

require("dotenv").config();

async function main() {
	let jwtSecret = process.env.JWT_SECRET;

	if (!jwtSecret) {
		jwtSecret = await ask("JWT Secret");
	}

	let payload = {};

	let env = await ask("Hostname or prod|dev|local [local]");
	payload.aud = env;
	switch (env) {
		case undefined:
		case "":
		case "local":
			payload.aud = "localhost";
			break;
		case "prod":
			payload.aud =
				"https://development-lms-brightspace-rest-api.k8s.its.maine.edu";
			break;
		case "dev":
			payload.aud =
				"https://1515-review-develop-3zknud.kubernetes-test.its.maine.edu";
	}

	payload.sub = await ask("Username");
	payload.iss = os.hostname();

	console.log("BEGIN BELOW THIS LINE");
	console.log(jwt.sign(payload, jwtSecret));
	console.log("END ABOVE THIS LINE");
}

function ask(question) {
	console.log(question);
	return new Promise((resolve, reject) => {
		process.stdin.resume();
		process.stdin.on("data", function (data) {
			process.stdin.pause();
			resolve(data.toString().trim());
		});
	});
}

main();
