const express = require("express");
const router = express.Router();
const Role = require("../controllers/role");
const logger = require("../services/logger");

router.post("/", function (req, res) {
	Role.setRole(req.body)
		.then(function () {
			logger.info("Role-Post Set user role ", {
				route: "role",
				method: "post",
				result: "success",
				userLmsId: req.body.userLmsId,
				role: req.body.role,
			});

			res.status(200).json({});
		})
		.catch(function (err) {
			logger.info("Role-Post error ", {
				route: "role",
				method: "post",
				result: "error",
				reqBody: req.body,
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

router.get("/system/", function (req, res) {
	Role.getAllRoles()
		.then(function (roles) {
			logger.info("Role-Get Get all roles", {
				route: "role",
				method: "get",
				result: "success",
			});

			res.status(200).json(roles);
		})
		.catch(function (err) {
			logger.info("Role-Get error", {
				route: "role",
				method: "get",
				result: "error",
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

module.exports = router;
