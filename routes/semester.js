const express = require("express");
const router = express.Router();
const orgController = require("../controllers/organization");
const logger = require("../services/logger");

router.post("/", function (req, res) {
	orgController
		.isOrgPresent(process.env.LMS_UMS_ORG_ID, "semester", req.body.code)
		.then(function (id) {
			if (id !== null) {
				logger.info("Semester-Post already exists", {
					id,
					route: "semester",
					method: "post",
					result: "exists",
					code: req.body.code,
				});
				return id;
			} else {
				const org = {
					name: req.body.name,
					code: req.body.code,
				};
				return orgController.add(process.env.LMS_UMS_ORG_ID, "semester", org);
			}
		})
		.then(function (id) {
			logger.info("Semester-Post success: ", {
				id,
				route: "semester",
				method: "post",
				result: "success",
				code: req.body.code,
			});
			res.status(200).json({ id: id });
		})
		.catch(function (err) {
			logger.info("Semester-Post error: ", {
				reqBody: req.data,
				route: "semester",
				method: "post",
				result: "error",
				code: req.body.code,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

module.exports = router;
