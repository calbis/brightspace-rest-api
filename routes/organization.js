const express = require("express");
const router = express.Router();
const organizationController = require("../controllers/organization");
const logger = require("../services/logger");

router.post("/", function (req, res) {
	organizationController
		.isOrgPresent(req.body.parent, req.body.type, req.body.code)
		.then(function (id) {
			if (id !== null) {
				logger.info("Organization-Post already exists", {
					route: "organization",
					method: "post",
					result: "exists",
					orgType: req.body.type,
					code: req.body.code,
					parent: req.body.parent,
					id,
				});
				return id;
			} else {
				const data = {
					code: req.body.code,
					name: req.body.name,
				};
				return organizationController.add(req.body.parent, req.body.type, data);
			}
		})
		.then(function (id) {
			logger.info("Organization-Post success: ", {
				id,
				route: "organization",
				method: "post",
				result: "success",
				orgType: req.body.type,
				code: req.body.code,
				parent: req.body.parent,
			});
			res.status(200).json({ id: id });
		})
		.catch(function (err) {
			logger.info("Organization-Post error: ", {
				route: "organization",
				method: "post",
				result: "error",
				orgType: req.body.type,
				code: req.body.code,
				parent: req.body.parent,
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

module.exports = router;
