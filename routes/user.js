const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const enrollmentController = require("../controllers/enrollment");
const logger = require("../services/logger");

router.post("/", function (req, res) {
	userController
		.isUserPresent(req.body.userId)
		.then(function (id) {
			if (id !== null) {
				logger.info("User-Post already exists", {
					id,
					route: "user",
					method: "post",
					result: "exists",
					userId: req.body.userId,
				});
				return userController.update(id, req.body);
			} else {
				return userController.add(req.body);
			}
		})
		.then(function (id) {
			logger.info("User-Post success: ", {
				id,
				route: "user",
				method: "post",
				result: "success",
				userId: req.body.userId,
			});
			res.status(200).json({ id: id });
		})
		.catch(function (err) {
			logger.info("User-Post error: ", {
				reqBody: req.data,
				route: "user",
				method: "post",
				result: "error",
				userId: req.body.userId,
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

router.put("/:id", function (req, res) {
	let id = req.params.id;
	userController
		.update(id, req.body)
		.then(function (id) {
			logger.info("User-Put success: ", {
				id,
				route: "user",
				method: "put",
				result: "success",
			});
			res.status(200).json({ id: id });
		})
		.catch(function (err) {
			logger.info("User-Put error: ", {
				reqBody: req.data,
				route: "user",
				method: "put",
				result: "error",
				id,
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

router.get("/isAdmin/:id", function (req, res) {
	let id = req.params.id;
	let umsOrgId = process.env.LMS_UMS_ORG_ID;
	enrollmentController
		.getUserRoleId(umsOrgId, id)
		.then(function (roleId) {
			let adminRoles = process.env.LMS_ADMIN_ROLE_IDS;
			let data = {
				userId: id,
				orgUnitId: umsOrgId,
				roleId: "",
				isAdmin: false,
			};
			if (roleId !== null) {
				data.roleId = roleId.toString();
			}
			if (adminRoles.includes(String(roleId))) {
				data.isAdmin = true;
			}
			logger.info("User-IsAdmin success: ", {
				route: "user-is-admin",
				method: "get",
				result: "success",
				id,
			});
			res.status(200).json(data);
		})
		.catch(function (err) {
			logger.info("User-IsAdmin error: ", {
				reqBody: req.data,
				route: "user-is-admin",
				method: "get",
				result: "error",
				id,
				error: err,
			});
			res.status(400).json({
				errors: err,
			});
		});
});

router.get("/:id", function (req, res) {
	let id = req.params.id;
	if (isNaN(id) || !Number.isInteger(Number(id))) {
		res.status(400).json({
			error: "Invalid parameter, id",
		});

		return;
	}

	userController
		.getUser(id)
		.then(function (user) {
			logger.info("User-Get-By-Id success: ", {
				route: "user",
				method: "get",
				result: "success",
				id,
			});
			res.status(200).json(user);
		})
		.catch(function (err) {
			logger.info("User-Get-By-Id error: ", {
				route: "user",
				method: "get",
				result: "error",
				id,
				errors: err,
			});
			res.status(400).json({
				id: id,
				errors: err,
			});
		});
});

module.exports = router;
